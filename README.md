# Demo School Rank Application
This is a demo springboot application to Get the Schools Ranking information.


## Table of contents
* [General info](#general-info)
* [Screenshots](#screenshots)
* [Technologies](#technologies)
* [Setup](#setup)
* [Features](#features)
* [Status](#status)
* [Inspiration](#inspiration)
* [Contact](#contact)

## General info
This is a demo springboot application to Get the Schools Ranking information using REST API from SchoolDigger.com.
Currently application run with an embedded tomcat and activemq and an in-memmory H2 DB. 
Currently one GET call is supported to get the list of schools by a specifci state code. 
School ranking data is fetched from an external source schooldigger.com using Developer API account.

## Screenshots
![Example screenshot](./img/screenshot.png)

## Technologies
* Spring Boot
* Embedded ActiveMQ
* H2 in-memory database

## Setup: MAC OS
Describe how to install / setup your local environement / add link to demo version.
to run this application, 
* download this code: git clone https://mazeemyousaf@bitbucket.org/mazeemyousaf/schoolrank.git
* open terminal and run "mvn install && java -jar target/schoolrank-0.0.1-SNAPSHOT.jar"
* this will build the code first and, start the spring boot application with latest code.
Note: Java 8, Git should be installed already. 

## Code Examples
Once application is running:
To Get the List of Schools with ranking information:
Request:  URL: http://localhost:8080/schools/{state-code}/
Response: if requested state data already exist, applcaiton will return the list of schools data in JSON format, 
incase of no data in local DB, Application will retreive the data from schooldigger.com API. and  
## Features
List of features ready and TODOs for future development
* Awesome feature 1
* Awesome feature 2
* Awesome feature 3

To-do list:
* Web App to consume and school's data and display on UI
* Develop UI in Angular or PHP

## Status
Project is: _in progress_,needs to work on developing UI to view data in user freindly format?


## Contact
Created by azeem.yousaf@gmail.com - feel free to contact me!