package com.myorg.schoolrank.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RankHistoryDto {
    private int rankStars;
    private int year;
    private String rankLevel;
    private int rank;
    private double averageStandardScore;

    public int getRankStars() {
        return rankStars;
    }

    public void setRankStars(int rankStars) {
        this.rankStars = rankStars;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getRankLevel() {
        return rankLevel;
    }

    public void setRankLevel(String rankLevel) {
        this.rankLevel = rankLevel;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public double getAverageStandardScore() {
        return averageStandardScore;
    }

    public void setAverageStandardScore(double averageStandardScore) {
        this.averageStandardScore = averageStandardScore;
    }
}
