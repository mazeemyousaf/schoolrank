package com.myorg.schoolrank.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CountyDto {
    private String countyURL;
    private String countyName;

    public String getCountyURL() {
        return countyURL;
    }

    public void setCountyURL(String countyURL) {
        this.countyURL = countyURL;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }
}
