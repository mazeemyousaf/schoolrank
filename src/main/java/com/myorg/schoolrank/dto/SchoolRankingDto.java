package com.myorg.schoolrank.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SchoolRankingDto {

    private int numberOfPages;
    private int numberOfSchools;
    private List<SchoolDto> schoolList;

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public int getNumberOfSchools() {
        return numberOfSchools;
    }

    public void setNumberOfSchools(int numberOfSchools) {
        this.numberOfSchools = numberOfSchools;
    }

    public List<SchoolDto> getSchoolList() {
        return schoolList;
    }

    public void setSchoolList(List<SchoolDto> schoolList) {
        this.schoolList = schoolList;
    }
}
