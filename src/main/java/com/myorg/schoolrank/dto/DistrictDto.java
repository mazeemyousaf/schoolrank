package com.myorg.schoolrank.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DistrictDto {
    private String districtID;
    private String districtName;
    private String url;
    private String rankURL;

    public String getDistrictID() {
        return districtID;
    }

    public void setDistrictID(String districtID) {
        this.districtID = districtID;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRankURL() {
        return rankURL;
    }

    public void setRankURL(String rankURL) {
        this.rankURL = rankURL;
    }
}
