package com.myorg.schoolrank.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SchoolDto {
    private AddressDto address;
    private CountyDto county;
    private DistrictDto district;
    private List<SchoolYearlyDetailsDto> schoolYearlyDetails;
    private List<RankHistoryDto> rankHistory;

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }

    public CountyDto getCounty() {
        return county;
    }

    public void setCounty(CountyDto county) {
        this.county = county;
    }

    public DistrictDto getDistrict() {
        return district;
    }

    public void setDistrict(DistrictDto district) {
        this.district = district;
    }

    public List<SchoolYearlyDetailsDto> getSchoolYearlyDetails() {
        return schoolYearlyDetails;
    }

    public void setSchoolYearlyDetails(List<SchoolYearlyDetailsDto> schoolYearlyDetails) {
        this.schoolYearlyDetails = schoolYearlyDetails;
    }

    public List<RankHistoryDto> getRankHistory() {
        return rankHistory;
    }

    public void setRankHistory(List<RankHistoryDto> rankHistory) {
        this.rankHistory = rankHistory;
    }

    private String schoolid;

    private Boolean isPrivate;

    private String schoolName;

    private String urlCompare;

    private String schoolLevel;

    private String lowGrade;

    private String rankMovement;

    private String url;

    private String phone;

    private String highGrade;


    public String getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(String schoolid) {
        this.schoolid = schoolid;
    }

    public Boolean getPrivate() {
        return isPrivate;
    }

    public void setPrivate(Boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getUrlCompare() {
        return urlCompare;
    }

    public void setUrlCompare(String urlCompare) {
        this.urlCompare = urlCompare;
    }

    public String getSchoolLevel() {
        return schoolLevel;
    }

    public void setSchoolLevel(String schoolLevel) {
        this.schoolLevel = schoolLevel;
    }

    public String getLowGrade() {
        return lowGrade;
    }

    public void setLowGrade(String lowGrade) {
        this.lowGrade = lowGrade;
    }

    public String getRankMovement() {
        return rankMovement;
    }

    public void setRankMovement(String rankMovement) {
        this.rankMovement = rankMovement;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getHighGrade() {
        return highGrade;
    }

    public void setHighGrade(String highGrade) {
        this.highGrade = highGrade;
    }

    @Override
    public String toString() {
        return "ClassPojo [county = " + county + ", isPrivate = " + isPrivate + ", schoolYearlyDetails = " + schoolYearlyDetails
                + ", schoolName = " + schoolName + ", urlCompare = " + urlCompare + ", schoolLevel = " + schoolLevel + ", rankHistory = "
                + rankHistory + ", lowGrade = " + lowGrade + ", address = " + address + ", rankMovement = " + rankMovement + ", url = "
                + url + ", phone = " + phone + ", highGrade = " + highGrade + ", schoolid = " + schoolid + ", district = " + district + "]";
    }
}
