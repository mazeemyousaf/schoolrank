package com.myorg.schoolrank;

import com.myorg.schoolrank.messaging.SchoolDataLoaderAsync;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListenerConfigurer;
import org.springframework.jms.config.JmsListenerEndpointRegistrar;
import org.springframework.jms.config.SimpleJmsListenerEndpoint;

@SpringBootApplication
@EnableJms
public class SchoolRankApplication implements JmsListenerConfigurer {

    @Value("${queue.name}")
    private String queueName;

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Autowired
    SchoolDataLoaderAsync schoolDataLoaderAsync;

    public static void main(String[] args) {
        SpringApplication.run(SchoolRankApplication.class, args);
    }

    @Override
    public void configureJmsListeners(JmsListenerEndpointRegistrar jmsListenerEndpointRegistrar) {
        SimpleJmsListenerEndpoint endpoint = new SimpleJmsListenerEndpoint();
        endpoint.setId("myQueueId");
        endpoint.setDestination(queueName);
        endpoint.setMessageListener(schoolDataLoaderAsync);
        jmsListenerEndpointRegistrar.registerEndpoint(endpoint);
    }
}
