package com.myorg.schoolrank.application;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myorg.schoolrank.domain.School;
import com.myorg.schoolrank.exception.SchoolDataNotAvailableException;
import com.myorg.schoolrank.messaging.SchoolDataLoadMessage;
import com.myorg.schoolrank.messaging.SchoolDataLoaderAsync;
import com.myorg.schoolrank.repository.SchoolRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class SchoolRankingService {
    @Value("${queue.name}")
    private String queueName;
    @Value("${list.of.supported.states}")
    List<String> supportedStates;

    @Autowired
    private SchoolRepository schoolRepository;
    @Autowired
    private SchoolDataLoaderAsync schoolDataLoaderAsync;

    ObjectMapper jsonMapper = new ObjectMapper();

    public List<School> getAllSchoolsByStateCode(String stateCode) throws SchoolDataNotAvailableException, JsonProcessingException {
        if(!supportedStates.contains(stateCode)){
            throw new SchoolDataNotAvailableException("School Ranking information not supported for the requested state");
        }
        List<School> allSchoolsByState = schoolRepository.findByAddressStateIgnoreCase(stateCode);
        if (CollectionUtils.isEmpty(allSchoolsByState)) {
            getStateSchoolDataAsync(stateCode);
            throw new SchoolDataNotAvailableException("No information available for the requested state, " +
                    "we have are preparing the information meanwhile, please check few minutes later");
        }
        return allSchoolsByState;
    }

    public List<School> getAllSchoolsByZipCode(String state, String zipCode) throws SchoolDataNotAvailableException, JsonProcessingException {
        List<School> allSchoolsByZipCode = schoolRepository.findByAddressZipIgnoreCase(zipCode);
        if (CollectionUtils.isEmpty(allSchoolsByZipCode)) {
            getStateSchoolDataAsync(state);
            throw new SchoolDataNotAvailableException("No information available for the requested state, " +
                    "we have are preparing the information meanwhile, please check few minutes later");
        }
        return allSchoolsByZipCode;
    }

    private void getStateSchoolDataAsync(String stateCode) throws JsonProcessingException {

        schoolDataLoaderAsync.send(queueName, jsonMapper.writeValueAsString(new SchoolDataLoadMessage(stateCode, null)));
    }
}
