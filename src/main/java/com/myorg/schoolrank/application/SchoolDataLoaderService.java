package com.myorg.schoolrank.application;

import com.myorg.schoolrank.domain.SchoolRankingData;
import com.myorg.schoolrank.dto.SchoolRankingDto;
import com.myorg.schoolrank.messaging.SchoolDataLoadMessage;
import com.myorg.schoolrank.repository.SchoolRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

@Service
public class SchoolDataLoaderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SchoolDataLoaderService.class);

    @Value("${school-api.url}")
    private String SCHOOLS_API_URL;
    @Value("${school-api.id}")
    private String SCHOOL_API_ID;
    @Value("${school-api.key}")
    private String SCHOOL_API_KEY;
    @Autowired
    private SchoolRepository schoolRepository;

    @Autowired
    private ModelMapper modelMapper;

    public void loadSchoolData(SchoolDataLoadMessage message) {
        try {
            SchoolRankingData schoolRankingData = getSchoolRankingDataFromThirdParty(message);
            if (!CollectionUtils.isEmpty(schoolRankingData.getSchoolList())) {
                schoolRepository.saveAll(schoolRankingData.getSchoolList());
                LOGGER.info("School Data Saved for requested state: "+message.getStateCode());
            }
        } catch (Exception ex) {
            LOGGER.error("error loading School Data from ASchool APi and saving to DB, reason:" + ex.getMessage(), ex);
        }
    }

    private SchoolRankingData getSchoolRankingDataFromThirdParty(SchoolDataLoadMessage message) {
        StringBuilder url=new StringBuilder(SCHOOLS_API_URL  + "?page=1&perPage=50&appID=" + SCHOOL_API_ID + "&appKey=" + SCHOOL_API_KEY);
        url.append("&st=" + message.getStateCode());
        //url.append("?zip=" + message.getStateCode());

        RestTemplate restTemplate = new RestTemplate();
        LOGGER.info("Going to get School Data using API: " + url);
        SchoolRankingDto schoolRankingDto = restTemplate.getForObject(url.toString(), SchoolRankingDto.class);
        LOGGER.info("School Data received from API, schoolRankingDto: "+schoolRankingDto);
        return modelMapper.map(schoolRankingDto, SchoolRankingData.class);
    }
}
