package com.myorg.schoolrank.repository;

import com.myorg.schoolrank.domain.Address;
import com.myorg.schoolrank.domain.School;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SchoolRepository extends CrudRepository<School, Long> {

    public List<School> findByAddressStateIgnoreCase(String state);
    public List<School> findByAddressZipIgnoreCase(String zipcode);
}
