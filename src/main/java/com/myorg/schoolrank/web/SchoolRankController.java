package com.myorg.schoolrank.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.myorg.schoolrank.application.SchoolRankingService;
import com.myorg.schoolrank.domain.School;
import com.myorg.schoolrank.domain.SchoolRankingData;
import com.myorg.schoolrank.dto.SchoolRankingDto;
import com.myorg.schoolrank.exception.SchoolDataNotAvailableException;
import com.myorg.schoolrank.messaging.SchoolDataLoaderAsync;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class SchoolRankController {

    @Autowired
    SchoolRankingService schoolRankingService;

    @Autowired
    SchoolDataLoaderAsync schoolDataLoaderAsync;

    @Autowired
    ModelMapper modelMapper;

    @GetMapping("/schools/{stateCode}")
    public SchoolRankingDto getAllSchools(@PathVariable String stateCode) throws SchoolDataNotAvailableException, JsonProcessingException {
        List<School> allSchoolsByStateCode = schoolRankingService.getAllSchoolsByStateCode(stateCode);
        SchoolRankingData schoolRankingData = new SchoolRankingData();
        schoolRankingData.setSchoolList(allSchoolsByStateCode);
        return modelMapper.map(schoolRankingData, SchoolRankingDto.class);
    }

    @GetMapping("/schools/{stateCode}/{zipcode}")
    public SchoolRankingDto getAllSchoolsByZipCode(@PathVariable String stateCode,@PathVariable String zipcode) throws SchoolDataNotAvailableException, JsonProcessingException {
        List<School> allSchoolsByStateCode = schoolRankingService.getAllSchoolsByZipCode(stateCode,zipcode);
        SchoolRankingData schoolRankingData = new SchoolRankingData();
        schoolRankingData.setSchoolList(allSchoolsByStateCode);
        return modelMapper.map(schoolRankingData, SchoolRankingDto.class);
    }

    @RequestMapping(value = "/health")
    public ResponseEntity health() {
        HttpStatus status;
        if (schoolDataLoaderAsync.isUp()) {
            status = HttpStatus.OK;
        } else {
            status = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(status);
    }

    //...
    @ExceptionHandler({ SchoolDataNotAvailableException.class })
    public void handleException(SchoolDataNotAvailableException ex,HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.NOT_FOUND.value(),ex.getMessage());
    }
}
