package com.myorg.schoolrank.domain;

import javax.persistence.*;
import java.util.List;


@Entity
public class County {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String countyURL;

    private String countyName;

    @OneToMany(fetch = FetchType.LAZY)
    private List<School> schools;

    public County() {
    }

    public String getCountyURL ()
    {
        return countyURL;
    }

    public void setCountyURL (String countyURL)
    {
        this.countyURL = countyURL;
    }

    public String getCountyName ()
    {
        return countyName;
    }

    public void setCountyName (String countyName)
    {
        this.countyName = countyName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [countyURL = "+countyURL+", countyName = "+countyName+"]";
    }

    public List<School> getSchools() {
        return schools;
    }

    public void setSchools(List<School> schools) {
        this.schools = schools;
    }
}
