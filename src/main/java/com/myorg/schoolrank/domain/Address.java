package com.myorg.schoolrank.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class Address {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String zip;

    private String cityURL;

    private String city;

    private String stateFull;

    private String zipURL;

    private String street;

    private String zip4;

    private String html;

    private String state;

    public Address() {
    }

    @OneToMany(fetch = FetchType.LAZY)
    private List<School> schools;

    public String getZip ()
    {
        return zip;
    }

    public void setZip (String zip)
    {
        this.zip = zip;
    }

    public String getCityURL ()
    {
        return cityURL;
    }

    public void setCityURL (String cityURL)
    {
        this.cityURL = cityURL;
    }

    public String getCity ()
    {
        return city;
    }

    public void setCity (String city)
    {
        this.city = city;
    }

    public String getStateFull ()
    {
        return stateFull;
    }

    public void setStateFull (String stateFull)
    {
        this.stateFull = stateFull;
    }

    public String getZipURL ()
    {
        return zipURL;
    }

    public void setZipURL (String zipURL)
    {
        this.zipURL = zipURL;
    }

    public String getStreet ()
    {
        return street;
    }

    public void setStreet (String street)
    {
        this.street = street;
    }

    public String getZip4 ()
    {
        return zip4;
    }

    public void setZip4 (String zip4)
    {
        this.zip4 = zip4;
    }

    public String getHtml ()
    {
        return html;
    }

    public void setHtml (String html)
    {
        this.html = html;
    }

    public String getState ()
    {
        return state;
    }

    public void setState (String state)
    {
        this.state = state;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [zip = "+zip+", cityURL = "+cityURL+", city = "+city+", stateFull = "+stateFull+", zipURL = "+zipURL+", street = "+street+", zip4 = "+zip4+", html = "+html+", state = "+state+"]";
    }

    public List<School> getSchools() {
        return schools;
    }

    public void setSchools(List<School> schools) {
        this.schools = schools;
    }
}
