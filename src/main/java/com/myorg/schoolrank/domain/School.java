package com.myorg.schoolrank.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class School
{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @ManyToOne(cascade = {CascadeType.ALL})
    private Address address;

    @ManyToOne(cascade = {CascadeType.ALL})
    private County county;

    @ManyToOne(cascade = {CascadeType.ALL})
    private District district;

    @OneToMany(mappedBy = "school", cascade = {CascadeType.ALL})
    private List<SchoolYearlyDetails> schoolYearlyDetails;

    @OneToMany(mappedBy = "school",cascade = {CascadeType.ALL})
    private List<RankHistory> rankHistory;

    private String schoolid;

    private Boolean isPrivate;

    private String schoolName;

    private String urlCompare;

    private String schoolLevel;

    private String lowGrade;

    private String rankMovement;

    private String url;

    private String phone;

    private String highGrade;

    public School(long id) {
        this.id = id;
    }

    public School() {
    }

    public String getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(String schoolid) {
        this.schoolid = schoolid;
    }

    public County getCounty() {
        return county;
    }

    public void setCounty(County county) {
        this.county = county;
    }

    public Boolean getPrivate() {
        return isPrivate;
    }

    public void setPrivate(Boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public List<SchoolYearlyDetails> getSchoolYearlyDetails() {
        return schoolYearlyDetails;
    }

    public void setSchoolYearlyDetails(List<SchoolYearlyDetails> schoolYearlyDetails) {
        this.schoolYearlyDetails = schoolYearlyDetails;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getUrlCompare() {
        return urlCompare;
    }

    public void setUrlCompare(String urlCompare) {
        this.urlCompare = urlCompare;
    }

    public String getSchoolLevel() {
        return schoolLevel;
    }

    public void setSchoolLevel(String schoolLevel) {
        this.schoolLevel = schoolLevel;
    }

    public List<RankHistory> getRankHistory() {
        return rankHistory;
    }

    public void setRankHistory(List<RankHistory> rankHistory) {
        this.rankHistory = rankHistory;
    }

    public String getLowGrade() {
        return lowGrade;
    }

    public void setLowGrade(String lowGrade) {
        this.lowGrade = lowGrade;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getRankMovement() {
        return rankMovement;
    }

    public void setRankMovement(String rankMovement) {
        this.rankMovement = rankMovement;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getHighGrade() {
        return highGrade;
    }

    public void setHighGrade(String highGrade) {
        this.highGrade = highGrade;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [county = "+county+", isPrivate = "+isPrivate+", schoolYearlyDetails = "+schoolYearlyDetails
                +", schoolName = "+schoolName +", urlCompare = "+urlCompare+", schoolLevel = "+schoolLevel+", rankHistory = "
                +rankHistory+", lowGrade = "+lowGrade+", address = "+address+", rankMovement = "+rankMovement+", url = "
                +url+", phone = "+phone+", highGrade = "+highGrade+", schoolid = "+schoolid+", district = "+district+"]";
    }
}