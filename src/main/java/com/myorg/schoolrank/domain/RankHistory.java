package com.myorg.schoolrank.domain;

import javax.persistence.*;

@Entity
public class RankHistory {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private int rankStars;

    private int year;

    private String rankLevel;

    private int rank;

    private double averageStandardScore;

    @ManyToOne(fetch = FetchType.LAZY)
    private School school;

    public RankHistory() {
    }

    public int getRankStars() {
        return rankStars;
    }

    public void setRankStars(int rankStars) {
        this.rankStars = rankStars;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getRankLevel() {
        return rankLevel;
    }

    public void setRankLevel(String rankLevel) {
        this.rankLevel = rankLevel;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public double getAverageStandardScore() {
        return averageStandardScore;
    }

    public void setAverageStandardScore(double averageStandardScore) {
        this.averageStandardScore = averageStandardScore;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [rankStars = "+rankStars+", year = "+year+", rankLevel = "+rankLevel+", rank = "+rank+", averageStandardScore = "+averageStandardScore+"]";
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }
}
