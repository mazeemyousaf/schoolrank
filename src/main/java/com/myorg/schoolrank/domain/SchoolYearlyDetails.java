package com.myorg.schoolrank.domain;

import javax.persistence.*;

@Entity
public class SchoolYearlyDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private int year;
    private double pupilTeacherRatio;
    private int numberOfStudents;
    private int numberofWhiteStudents;
    private double percentofWhiteStudents;
    private int numberofAsianStudents;
    private double percentofAsianStudents;

    public SchoolYearlyDetails() {
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    private School school;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getPupilTeacherRatio() {
        return pupilTeacherRatio;
    }

    public void setPupilTeacherRatio(double pupilTeacherRatio) {
        this.pupilTeacherRatio = pupilTeacherRatio;
    }

    public int getNumberOfStudents() {
        return numberOfStudents;
    }

    public void setNumberOfStudents(int numberOfStudents) {
        this.numberOfStudents = numberOfStudents;
    }

    public int getNumberofWhiteStudents() {
        return numberofWhiteStudents;
    }

    public void setNumberofWhiteStudents(int numberofWhiteStudents) {
        this.numberofWhiteStudents = numberofWhiteStudents;
    }

    public double getPercentofWhiteStudents() {
        return percentofWhiteStudents;
    }

    public void setPercentofWhiteStudents(double percentofWhiteStudents) {
        this.percentofWhiteStudents = percentofWhiteStudents;
    }

    public int getNumberofAsianStudents() {
        return numberofAsianStudents;
    }

    public void setNumberofAsianStudents(int numberofAsianStudents) {
        this.numberofAsianStudents = numberofAsianStudents;
    }

    public double getPercentofAsianStudents() {
        return percentofAsianStudents;
    }

    public void setPercentofAsianStudents(double percentofAsianStudents) {
        this.percentofAsianStudents = percentofAsianStudents;
    }

    @Override
    public String toString() {
        return "ClassPojo [numberofWhiteStudents = " + numberofWhiteStudents + ", year = " + year + ", , pupilTeacherRatio = "
                + pupilTeacherRatio + ", numberofAsianStudents = " + numberofAsianStudents + ", numberOfStudents = "
                + numberOfStudents + ", percentofWhiteStudents = " + percentofWhiteStudents + ", percentofAfricanAmericanStudents = "
                + ", percentofAsianStudents = " + percentofAsianStudents + "]";
    }
}
