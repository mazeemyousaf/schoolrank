package com.myorg.schoolrank.domain;

import java.util.List;

public class SchoolRankingData {

    List<School> schoolList;

    public SchoolRankingData() {
    }

    public List<School> getSchoolList() {
        return schoolList;
    }

    public void setSchoolList(List<School> schoolList) {
        this.schoolList = schoolList;
    }
}
