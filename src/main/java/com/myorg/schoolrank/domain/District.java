package com.myorg.schoolrank.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class District {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String districtID;

    private String districtName;

    private String url;

    private String rankURL;

    @OneToMany(fetch = FetchType.LAZY)
    private List<School> schools;

    public District() {
    }

    public String getDistrictID ()
    {
        return districtID;
    }

    public void setDistrictID (String districtID)
    {
        this.districtID = districtID;
    }

    public String getDistrictName ()
    {
        return districtName;
    }

    public void setDistrictName (String districtName)
    {
        this.districtName = districtName;
    }

    public String getUrl ()
    {
        return url;
    }

    public void setUrl (String url)
    {
        this.url = url;
    }

    public String getRankURL ()
    {
        return rankURL;
    }

    public void setRankURL (String rankURL)
    {
        this.rankURL = rankURL;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [districtID = "+districtID+", districtName = "+districtName+", url = "+url+", rankURL = "+rankURL+"]";
    }

    public List<School> getSchools() {
        return schools;
    }

    public void setSchools(List<School> schools) {
        this.schools = schools;
    }
}
