package com.myorg.schoolrank.messaging;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myorg.schoolrank.application.SchoolDataLoaderService;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.jms.*;
import java.util.Collections;

@Component
public class SchoolDataLoaderAsync implements MessageListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(SchoolDataLoaderAsync.class);

    @Autowired
    private JmsTemplate jmsTemplate;

    ObjectMapper jsonMapper = new ObjectMapper();

    @Autowired
    private SchoolDataLoaderService schoolDataLoaderService;

    private int counter = 0;

    public void send(String destination, String message) {
        LOGGER.info("sending message='{}' to destination='{}'", message, destination);
        jmsTemplate.convertAndSend(destination, message);
    }

    public int pendingJobs(String queueName) {
        return jmsTemplate.browse(queueName, (s, qb) -> Collections.list(qb.getEnumeration()).size());
    }

    public boolean isUp() {
        ConnectionFactory connection = jmsTemplate.getConnectionFactory();
        try {
            connection.createConnection().close();
            return true;
        } catch (JMSException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onMessage(Message message) {
        LOGGER.info("received message:"+message);
        try {
            String jsonMessageText=((TextMessage)message).getText();
            schoolDataLoaderService.loadSchoolData(jsonMapper.readValue(jsonMessageText,SchoolDataLoadMessage.class));
        } catch (Exception e) {
           LOGGER.error(e.getMessage(),e);
        }
    }
}
