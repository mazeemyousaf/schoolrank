package com.myorg.schoolrank.messaging;


public class SchoolDataLoadMessage {
    private String stateCode;
    private String zipcode;

    public SchoolDataLoadMessage(){}

    public SchoolDataLoadMessage(String stateCode, String zipcode) {
        this.stateCode = stateCode;
        this.zipcode = zipcode;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }
}
