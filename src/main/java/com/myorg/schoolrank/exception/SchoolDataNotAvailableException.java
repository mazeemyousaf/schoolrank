package com.myorg.schoolrank.exception;

public class SchoolDataNotAvailableException extends Throwable {
    public SchoolDataNotAvailableException(String message) {
        super(message);
    }
}
